#!/usr/bin/nodejs

console.log('starting');

const config = {
	wsServerPort: 8888
};

//...app connectivity.......................................................
const clients = [];
let clientCount = 0;

function getNextClientId() {
	console.log('getNextClientId');
	return clientCount++;
}

//install routes for inbound, client-sourced messages
const msgHandlers = {};
	
function installHandler(key, handler){
	console.log('adding handler for ' + key);
		
	msgHandlers[key] = handler;
}

installHandler('chat', function(client, data) { 
	console.log('received chat from clientId ' + client.clientId);
	var data = data || {};

	const rsp = JSON.stringify({ type: 'chat', text: data.text });
	clients.forEach(c => c != client && c.socket.sendUTF(rsp));
});

installHandler('ping', function(client, data) { 
	console.log('received ping from clientId ' + client.clientId);

	client.socket.sendUTF(JSON.stringify({ type: 'pong' }));
});

//...websocket server..................................................
var WebSocketServer = require('websocket').server;
var http = require('http');

var server = http.createServer(function (req, res) {});

server.listen(config.wsServerPort, function() {
    console.log((new Date()) + ' websocket server is listening on port ' + config.wsServerPort);
});

wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});

wsServer.on('request', function(request) {
	const socket = request.accept('', request.origin);
	
	const client = { 
		socket: socket, 
		clientId: getNextClientId()
	};

	clients.push(client);

    console.log((new Date()) + ' Connection accepted.');
			
    socket.on('message', function(message) {
        if (message.type === 'utf8') {
            console.log('Received Message: ' + message.utf8Data);
            
			const received = JSON.parse(message.utf8Data);

				msgHandlers[received.type]
			&&	msgHandlers[received.type](client, received);	
        }
    });
    
	socket.on('close', function(reasonCode, description) {
		console.log((new Date()) + ' Peer ' + socket.remoteAddress + ' disconnected.');
		
		const i = clients.findIndex(c => c === client);
		clients.splice(i, 1);
    });
});
