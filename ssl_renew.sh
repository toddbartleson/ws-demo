#!/bin/bash

# 0 12 * * * /mnt/volume_sfo2_02/ssl_renew.sh >> /var/log/cron.log 2>&1

COMPOSE="/usr/local/bin/docker-compose --no-ansi"
DOCKER="/usr/bin/docker"

cd /root
$COMPOSE run certbot renew && $COMPOSE kill -s SIGHUP webserver
$DOCKER system prune -af
