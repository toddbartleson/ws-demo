var protocol = (function () {
    "use strict"

    console.log('init protocol');

	var config = {	
		connectionAttemptWindow: 15000,		
		reconnect: true,			
		reconnectionAttemptInterval: 100, //ms			
		maxReconnectionAttempts: 0,			
		minReconnectionAttemptDelay: 5000,
		delayPerReconnectionAttempt: 5000 //ms			
	};
		
	var url = null;
    
    var connecting = false;
	var reconnecting = false;		
    var connected = false;
    var socket = null;
    var connectionTimer = null;
	var reconnectionAttempts = 0;
		
    var handlers = {};

    var connectionCallback;
    var disconnectionCallback;

	var strings = {
        'no-connection-error': 'No connection or Not connecting.',
        'connection-error': 'Disconnect or connection error.',
        'message-not-json': 'Malformed message, not valid json.',
        'message-no-type': 'Malformed message, type not defined.',
        'message-no-handler': 'Unhandled message, handler not defined.'
    };
    
    function addHandlers (_handlers) {
        for (var key in _handlers) {
            handlers[key] = _handlers[key];
        }
    }

    function reset () {
        console.log('protocol.reset');

        //reset connected and socket
        connecting = false;
        connected = false;
        socket = null;
    }

    //connection
    //where url is like ws://sub.domain.com/api/socket/client");
    function connect(_url, _connectionCallback, _disconnectionCallback) {
		if (connecting) {
			console.log('protocol.connect re-entered');
			return;
		}
		
		console.log('protocol.connect');

		//set flag to prevent re-enterance
		connecting = true;			
		reconnecting = false;
		
		if (typeof (_url) === 'string') {
			url = _url;
        }
        if (typeof (_connectionCallback) === 'function') {
			connectionCallback = _connectionCallback;
        }
        if (typeof (_disconnectionCallback) === 'function') {
			disconnectionCallback = _disconnectionCallback;
		}

        if (!socket) {
			try {
				socket = new WebSocket(url);
			}
			catch (ex) {
				handleError('no-connection-error', JSON.stringify(ex));
				return;
			}

            socket.onopen = handleConnection;           
            socket.onerror = function (err) {
                handleError('connection-error', err);
                
                //disconnect on connection error	
				setTimeout(disconnect, 100);
            };           
            socket.onmessage = handleMessage;
            socket.onclose = disconnect;
            
            //set a timer to connect, 
			//clear it if a connection is made
			//otherwise, if it elapses, then there is no connection so start over
			connectionTimer = setTimeout(disconnect, config.connectionAttemptWindow, { Error: 'Connection Timeout!' });
        }
    }

    function handleConnection() {
        if (socket.readyState == WebSocket.OPEN) {
            
            //if the connection is successful
			//clear connect attempt timer
			clearTimeout(connectionTimer);
			connectionTimer = null;
			
			//reset reconnection attempts
			reconnectionAttempts = 0;
           
            connected = true;
			connecting = false;		
	
            if (typeof(connectionCallback) === 'function') {
                connectionCallback();
            };
        }

        else {
            handleError('no-connection-error', 'socket.readyState = ' + socket.readyState);
            
            //disconnect on connection error	
			setTimeout(disconnect, 100);
        }
    };

    function handleMessage (msg) {       
        try {
            msg = JSON.parse(msg.data);
        }

        catch (ex) {
            handleError('message-not-json', msg.data);
            return;
        }

        if (!msg.type) {
            handleError('message-no-type', msg);
            return;
        }

        if (typeof handlers[msg.type] === 'function') {
            handlers[msg.type](msg);
            return;
        }

        handleError('message-no-handler', msg);
    };

    //where type is a string, and args is an object
    function send (type, msg) {
		if (!msg.type) {
			msg.type = type;
		}

        try {
            msg = JSON.stringify(msg);
        }

        catch (ex) {
            handleError('message-not-json', msg);
            return;
        }

        if (	connected 
			&&  socket 
			&&  socket.send) {
			socket.send(msg);     
		}   
    };

    function disconnect (event) {
        console.log('protocol.disconnect');
        
        if (event) {
			console.log(JSON.stringify(event));
		}

        //if you're connected close the socket
        if (	connected 
			&&  socket 
			&&  socket.close) {
			socket.close();
		}
        
        if (typeof(disconnectionCallback) === 'function') {
            disconnectionCallback();
        };
        
        if (config.reconnect) {
			//reconnect after a brief delay
			setTimeout(retryConnection, config.reconnectionAttemptInterval);
		}
    }

	function retryConnection() {
		if (reconnecting) {
			console.log('protocol.retryConnection re-entered');
			return;
		}
		
		console.log('!!! RECONNECT WebSocket client');
		
		//set flag to prevent re-enterance
		reconnecting = true;
		
		//reset the things
		reset();					
		
		//it can only go on so long
		if (	config.maxReconnectionAttempts
			&&	reconnectionAttempts > config.maxReconnectionAttempts) {
			//just stop
			console.log('!!! WebSocket client STOPPING will NOT reconnect!');
			return;
		}
					
		var connectionAttemptDelay = config.minReconnectionAttemptDelay +
			reconnectionAttempts * config.delayPerReconnectionAttempt;
				
		console.log(
			'!!! reconnect #' + (reconnectionAttempts + 1).toString() + 
			', reconnecting in ' + (connectionAttemptDelay / 1000).toString() + 
			' seconds'
		);
				
		setTimeout(connect, connectionAttemptDelay);	
		
		//track reconnection attempt
		reconnectionAttempts++;						
	}

    function handleError (errorId, msg) {
        console.log('!!! protocol error: ' + strings[errorId] + ' => ' + JSON.stringify(msg));
    }

    var publicApi = {
        addHandlers: addHandlers,      
        connect: connect,
        isConnected: function() { 
			return connected;
		},
        send: send,
        disconnect: disconnect
    };

    return publicApi;
})();