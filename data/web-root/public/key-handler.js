
//where config is map of functions like
//{ keyCode: handler, ... }
//for example 
//call keyHandler.install({
//	32: function(code, char, event) {
//		//do stuff
//	}
//})
var keyHandler = (function($) {	
	var handlers = {};
	var handleKeyPress = function(e){
		var e = e || window.event;
		var code = e.keyCode || e.which;
		var char = String.fromCharCode(code).toLowerCase();
		if (typeof(handlers[code]) === 'function') {
			handlers[code](code, char, e);
		}
	};
	var install = function(_handlers) {
		handlers = _handlers;
		$(window).bind('keydown', handleKeyPress);
	};

	var publicApi = {
		install: install
	};
	
	return publicApi;
})($);
