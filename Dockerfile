FROM node:13-alpine

WORKDIR /usr/src/app

COPY ops/start-online-node ops/start-online-node
COPY server server

RUN chmod +x ops/start-online-node

EXPOSE 8888
CMD [ "/usr/src/app/ops/start-online-node" ]
